/*
* Board.h
*
*  Created on: 14 feb 2014
*      Author: strimmel
*
*
* The board is build up by an 1 dimensional vector like this:
*      | Row |
* -----------+-------------------------------------
*  Col |     |   0   1   2   3   4   5   6   7   8
* -----------+-------------------------------------
*      |  0  |   0   1   2   3   4   5   6   7   8
*      |  1  |   9  10  11  12  13  14  15  16  17
*      |  2  |  18  19  20  21  22  23  24  25  26
*      |  3  |  27  28  29  30  31  32  33  34  35
*      |  4  |  36  37  38  39  40  41  42  43  44
*      |  5  |  45  46  47  48  49  50  51  52  53
*      |  6  |  54  55  56  57  58  59  60  61  62
*      |  7  |  63  64  65  66  67  68  69  70  71
*      |  8  |  72  73  74  75  76  77  78  79  80
*
*/

#ifndef BOARD_H_
#define BOARD_H_

#include <iostream>
#include <vector>
#include <string>
#include "Brick.h"


class Board {
private:
  std::vector<Brick> bricks;

  void create_bricks();

  virtual int calc_square(int row, int col);               // Virtual because of GMOCK
  virtual int first_pos_in_square(int row, int col);       // Virtual because of GMOCK
  virtual int first_pos_in_square(int square_nr);          // Virtual because of GMOCK
  virtual int vector_index_to_square(int vector_index);    // Virtual because of GMOCK

public:
  Board();
  virtual ~Board();                             // Virtual because of GMOCK
  virtual void set_all_brick_friends();         // Virtual because of GMOCK
  virtual std::string print_board();            // Virtual because of GMOCK
  virtual std::string print_board_all_values(); // Virtual because of GMOCK
  virtual std::vector<Brick>& get_bricks();     // Virtual because of GMOCK
  virtual bool solved();                        // Virtual because of GMOCK
  virtual void reset();                         // Virtual because of GMOCK
};

#endif /* BOARD_H_ */


