#include "RandomGenerator.h"

using namespace std;

RandomGenerator::RandomGenerator(Board& board, Solver& solver) : Generator(board), solver(solver) { }

RandomGenerator::~RandomGenerator() { }

bool RandomGenerator::get_board() {
  char p[82];
  memset(p, 48, 81); // Setting the char items to char(48) == '0'
  p[81] = 0;         // Adding '\0'
  srand((unsigned)time(NULL));  // Setting seed
  fill_board(p);

  // Resetting the board to the starting point (all bricks are reseted)
  board.reset();

  vector<int> positions_left;
  positions_left.reserve(81);
  for (int i = 0; i < 81; i++) {
    positions_left.push_back(i); // a vector with [index=0,value=0]..[index=80,value=80]
  }

  vector<Brick>& bricks = board.get_bricks();

  // First inserting 20 of the values
  for (int i = 0; i < 20; i++) {
    int rand_pos;
    do {
      rand_pos = rand() % 81;
    } while (positions_left[rand_pos] == -1);

    bricks[rand_pos].set_correct_value(p[rand_pos] - 48); // p is a char* with values '0'..'9' so -48 will give us the numeric integer 0..9
    positions_left[rand_pos] = -1; // Setting this position to -1 (meaning it has been used)
  }


  vector<Brick> unsolved_bricks(bricks); // Coping to unsolved_bricks because solver.solve() is a destroying function
  // Loop until we have a board that is possible to solve
  while (!solver.solve()) {

    // Add three values each time to the bricks in the board
    for (int i = 0; i < 3; i++) {
      int rand_pos;
      do {
        rand_pos = rand() % 81;
      } while (positions_left[rand_pos] == -1);

      bricks[rand_pos].set_correct_value(p[rand_pos] - 48);
      unsolved_bricks[rand_pos].set_correct_value(p[rand_pos] - 48);
      positions_left[rand_pos] = -1;  // Setting this position to -1 (meaning it has been used)
    }

  }

  // The bricks has been solved so we copy back the unsolved_bricks
  bricks = unsolved_bricks;

  // We need to calculate the return value before incrementing num_generated_boards
  bool return_val = !finished();

  // Increment number of generated boards
  num_generated_boards++;

  return return_val;
}

// This code is written by: Oli Charlesworth
// It randomly creates a entire board.
// It is not that easy to understand, but it seems to do its purpose.
int RandomGenerator::fill_board(char* v) {
  char*  r = v;
  char  i = 0, n = 0, k = 48;
  int  j, m = 0;

  while (*r>k) { 
    i++;
    r++;
    if (*r == 0) {
      return 1;
    }
  }

  k = (rand() % 9) + 49;  // Randomize a value in range 1..9
  while (n<9) {
    j = 81;
    while (j) {
      j--;
      if (r[j - i] - k) {
        m = 1;
      }
      else {
        m = 0;
        if (((i / 9) ^ (j / 9)) && ((i % 9) ^ (j % 9))) {
          if (((i / 27) ^ (j / 27)) || (((i % 9) / 3) ^ ((j % 9) / 3))) m = 1;
        }
      }
      *r *= m;
    }
    if (*r) {
      *r = k;
      if (fill_board(v)) return 1;
    }
    *r = 1;
    n++;
    if (++k == 58) k = 49;
  }
  return 0;
}
