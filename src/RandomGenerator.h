/*
* RandomGenerator.h
*
*  Created on: 24 feb 2014
*      Author: strimmel
*/

#ifndef RANDOMGENERATOR_H_
#define RANDOMGENERATOR_H_

#include <iostream>
#include <stdlib.h> // # srand
#include <stdio.h>  // # putc
#include <string.h> // # memset
#include <time.h>   // # time

#include "Generator.h"
#include "Board.h"
#include "Solver.h"


class RandomGenerator : public Generator {
private:
  Solver& solver;
  int fill_board(char* v);
public:
  RandomGenerator(Board& board, Solver& solver);
  virtual ~RandomGenerator();                      // Virtual because of GMOCK
  virtual bool get_board();                        // Virtual because of GMOCK
};

#endif /* RANDOMGENERATOR_H_ */
