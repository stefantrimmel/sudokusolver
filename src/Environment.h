/*
* Environment.h
*
*  Created on: 26 feb 2014
*      Author: strimmel
*/

#ifndef ENVIRONMENT_H_
#define ENVIRONMENT_H_


#include <iostream>
#include <string>
#include <memory>

#include "Board.h"
#include "Solver.h"
#include "InputFromFile.h"
#include "RandomGenerator.h"


class Environment {
private:
  std::string test_pattern_path;
  Board board;
  Solver solver;
  std::shared_ptr<Generator> p_generator; // Can be a RandonGenerator, InputFromFile or some type of generator 
  int board_index;
public:
  Environment(std::string test_pattern_path);
  virtual ~Environment();                      // Virtual because of GMOCK
  virtual void run();                          // Virtual because of GMOCK
};

#endif /* ENVIRONMENT_H_ */