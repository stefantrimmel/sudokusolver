/*
* Brick.cpp
*
*  Created on: 14 feb 2014
*      Author: strimmel
*/

#include "Brick.h"

using namespace std;

Brick::Brick(int row, int col) : row(row), col(col) {

  // Populate with 1..9
  for (int i = 1; i<10; i++) {
    possible_values.push_back(i);
  }

}

Brick::~Brick() { }

// #########################################
// ##### Functions operating on itself #####
// #########################################

void Brick::set_correct_value(int value) {
  possible_values.clear();
  possible_values.push_back(value);
}

// Just for debug, testing
void Brick::add_value(int value) {
  possible_values.push_back(value);
  possible_values.sort();
}

// Returns true if the value was removed
bool Brick::remove_value(int value) {
  unsigned int before_size = possible_values.size();
  possible_values.remove(value);
  return (possible_values.size() < before_size);
}

bool Brick::value_exists(int value) const {
  bool value_exists = false;
  for (const auto& list_val : possible_values) {
    if (list_val == value) {
      value_exists = true;
      break;
    }
    else if (list_val > value) {
      // The list is sorted so we don't need to search the entire list
      break;
    }
  }
  return value_exists;
}

bool Brick::solved() const {
  return (possible_values.size() == 1);
}

// If this one returns true then we either have
// a board with a false starting point OR that
// the solver has an error in the code.
bool Brick::no_possible_values() const {
  return (possible_values.size() == 0);
}

int Brick::get_value_int() const {
  int val = -1;
  if (possible_values.size() > 0) {
    val = possible_values.front();
  }
  return val;
}

string Brick::get_value_str() const {
  string s = "";

  if (possible_values.size() > 0) {
    ostringstream oss;
    oss << possible_values.front();
    s += oss.str();
  }

  return s;
}

string Brick::get_all_values_str() const {
  string s = "";

  if (possible_values.size() > 0) {
    ostringstream oss;
    int iterations = 0;
    for (const auto& it : possible_values) {
      iterations++;
      oss << it;
    }
    // Adding spaces so that size("123456789") == size("1234     ")
    for (int i = iterations; i < 9; i++) {
      oss << " ";
    }
    s += oss.str();
  }

  return s;
}

string Brick::get_name() const {
  ostringstream oss;
  oss << "Brick row: " << row << " col: " << col;
  string s = oss.str();
  return s;
}

// ##########################################
// ##### Functions operating on friends #####
// ##########################################

void Brick::add_square_friend(Brick* brick) {
  square_friends.push_back(brick);
}

void Brick::add_row_friend(Brick* brick) {
  row_friends.push_back(brick);
}

void Brick::add_col_friend(Brick* brick) {
  col_friends.push_back(brick);
}

bool Brick::remove_single_from_friends() {
  bool some_value_removed = false;

  // If this brick is solved then we can remove its value from the friends
  if (solved()) {
    int value = possible_values.front();
    some_value_removed = remove_value_from_friends(value);
  }

  return some_value_removed;
}

bool Brick::remove_value_from_friends(int value) {
  bool some_value_removed = false;
  some_value_removed |= remove_value_square_friends(value);
  some_value_removed |= remove_value_row_friends(value);
  some_value_removed |= remove_value_col_friends(value);
  return some_value_removed;
}

bool Brick::remove_value_square_friends(int value) {
  bool some_value_removed = false;
  for (auto& s_friend : square_friends) {
    some_value_removed |= s_friend->remove_value(value);
  }
  return some_value_removed;
}

bool Brick::remove_value_row_friends(int value) {
  bool some_value_removed = false;
  for (auto& r_friend : row_friends) {
    some_value_removed |= r_friend->remove_value(value);
  }
  return some_value_removed;
}

bool Brick::remove_value_col_friends(int value) {
  bool some_value_removed = false;
  for (auto& c_friend : col_friends) {
    some_value_removed |= c_friend->remove_value(value);
  }
  return some_value_removed;
}


bool Brick::find_hidden_single() {
  bool hidden_single_found = false;
  if (possible_values.size() > 1) {
    for (const auto& value : possible_values) {
      if (!value_exists_square_friends(value)) {
        set_correct_value(value);
        hidden_single_found = true;
        break;
      }
      if (!value_exists_row_friends(value)) {
        set_correct_value(value);
        hidden_single_found = true;
        break;
      }
      if (!value_exists_col_friends(value)) {
        set_correct_value(value);
        hidden_single_found = true;
        break;
      }
    } // for each possible value
  } // size > 1
  return hidden_single_found;
}

bool Brick::value_exists_in_friends(int value, const std::vector<Brick*>& p_friends) const {
  bool value_exists = false;
  for (const auto& p_friend : p_friends) {
    value_exists |= p_friend->value_exists(value);
    if (value_exists) break; // We don't need to search anymore if one match is found
  }
  return value_exists;
}

bool Brick::value_exists_square_friends(int value) const {
  bool value_exists = false;
  for (const auto& s_friend : square_friends) {
    value_exists |= s_friend->value_exists(value);
    if (value_exists) break; // We don't need to search anymore if one match is found
  }
  return value_exists;
}

bool Brick::value_exists_row_friends(int value) const {
  bool value_exists = false;
  for (const auto& r_friend : row_friends) {
    value_exists |= r_friend->value_exists(value);
    if (value_exists) break; // We don't need to search anymore if one match is found
  }
  return value_exists;
}

bool Brick::value_exists_col_friends(int value) const {
  bool value_exists = false;
  for (const auto& c_friend : col_friends) {
    value_exists |= c_friend->value_exists(value);
    if (value_exists) break; // We don't need to search anymore if one match is found
  }
  return value_exists;
}

// *********************************************************************************
// A naked pair is found when two cells have the same two values and only these
// values. Then these two values can be removed from the other cells in the group.
// group == row|col|square
// *********************************************************************************
bool Brick::find_naked_pair() {
  bool naked_pair_found = false;
  if (possible_values.size() == 2) {
    naked_pair_found |= remove_naked_pair_from_friends(square_friends);
    naked_pair_found |= remove_naked_pair_from_friends(row_friends);
    naked_pair_found |= remove_naked_pair_from_friends(col_friends);
  }
  return naked_pair_found;
}

bool Brick::remove_naked_pair_from_friends(vector<Brick*>& p_friends) {
  bool naked_pair_removed = false;
  bool a_naked_pair = false;
  for (auto& p_friend : p_friends) {
    // This friend brick needs to have 2 and only 2 possible values
    if (p_friend->possible_values.size() == 2) {
      a_naked_pair = true;
      for (auto& value : possible_values) {
        // a_naked_pair will be false if both values don't exist
        a_naked_pair &= p_friend->value_exists(value);
        if (!a_naked_pair) break; // Both values need to match
      }
      if (a_naked_pair) break; // There can only be one naked pair, no need to continue
    } // friend_brick_values.size() == 2
  } // for each friend

  // Remove these 2 values from the other bricks if a naked_pair was found
  if (a_naked_pair) {
    for (auto& p_friend : p_friends) {
      // This friend brick needs to have more than 2 possible values
      if (p_friend->possible_values.size() > 2) {
        for (auto& value : possible_values) {
          naked_pair_removed |= p_friend->remove_value(value);
        }
      } // friend_brick_values.size() > 2
    } // for each friend
  } // a naked pair

  return naked_pair_removed;
}

// *********************************************************************************
// A naked triple is found when three cells have the same values or a subset of
// these values. These values can then be removed from the other cells in the group
// group == row|col|square
// *********************************************************************************
bool Brick::find_naked_triple() {
  bool naked_triple_found = false;
  if (possible_values.size() == 2 || possible_values.size() == 3) {
    naked_triple_found |= remove_naked_triple_from_friends(square_friends);
    naked_triple_found |= remove_naked_triple_from_friends(row_friends);
    naked_triple_found |= remove_naked_triple_from_friends(col_friends);
  }
  return naked_triple_found;
}

bool Brick::remove_naked_triple_from_friends(std::vector<Brick*>& p_friends) {
  bool naked_triple_removed = false;
  int a_naked_triple = 1;
  int third_value_to_remove = -1;

  if (possible_values.size() == 3) {

    for (auto& p_friend : p_friends) {
      // The friend needs to have 2 or 3 values
      if (p_friend->possible_values.size() == 2 || p_friend->possible_values.size() == 3) {
        unsigned int at_least_two = 0;
        for (auto& value : possible_values) {
          if (p_friend->value_exists(value)) at_least_two++;
        }

        // It needs to be a subset or a perfect subset of at least 2 values
        if (at_least_two > 1 && p_friend->possible_values.size() == at_least_two) a_naked_triple++;

        if (a_naked_triple == 3) {
          third_value_to_remove = possible_values.back();
          break; // We don't need to continue the for each p_friend if we have found a_naked_tripple
        }
      } // friend value size 2 or 3
    } // for each friend

  } // if brick possible value size 3
  else if (possible_values.size() == 2) {

    // We are looking for this situation:
    //  1st brick  2nd brick  3rd brick
    //   +-----+    +-----+    +-----+
    //   | 1 6 |    | 1 4 |    | 4 6 |
    //   +-----+    +-----+    +-----+
    //   first_brick_matched_value: 1
    //   value_to_match: 6
    //              second_brick_not_matched: 4
    //                         3rd brick must match value_to_match and second_brick_not_matched

    for (auto p_friend = p_friends.begin(); p_friend != p_friends.end(); p_friend++) {
      // The friend needs to have 2 values
      if ((*p_friend)->possible_values.size() == 2) {
        int at_least_one = 0;
        int value_to_match = -1;

        for (auto& value : possible_values) {
          if ((*p_friend)->value_exists(value)) {
            at_least_one++;
          }
          else {
            value_to_match = value; // Saving the value that didn't match, this need to match with the last brick
          }
        }

        if (at_least_one == 1) {

          bool found_matching_brick = false;
          int second_brick_not_matched = -1;

          // Find the second bricks not matched value
          for (const auto& value : (*p_friend)->possible_values) {
            if (value_to_match != value) {
              second_brick_not_matched = value;
              break;
            }
          }

          // Checking if we are on the end of the list, we don't want to advance out of range
          int distance = abs(std::distance(p_friends.end(), p_friend));
          if (distance <= 1) break;
          // Starting with the position of p_friend iterator
          auto rest_of_friend(p_friend);
          // Advancing the rest_of_friend one step so that we don't check the already checked p_friend
          std::advance(rest_of_friend, 1);

          for (; rest_of_friend != p_friends.end(); rest_of_friend++) {
            if ((*rest_of_friend)->possible_values.size() == 2) {
              found_matching_brick = true;
              found_matching_brick &= (*rest_of_friend)->value_exists(second_brick_not_matched);
              found_matching_brick &= (*rest_of_friend)->value_exists(value_to_match);
              if (found_matching_brick) break;
            }
          }

          if (found_matching_brick) {
            // We found the last matching brick :)
            third_value_to_remove = second_brick_not_matched;
            a_naked_triple = 3;
          }
          else {
            // Revert back the iterator, because this was a false match
            //std::advance(p_friend, -saved_p_friend_position);
          }

        }
        else if (at_least_one == 2) {
          // This is not a naked triple, it is a naked pair
          break;
        }

        if (a_naked_triple == 3) {
          // We don't need to continue the for each p_friend if we have found a_naked_tripple
          break;
        }

      } // friend value size 2
    } // for each friend

  } // else if brick possible value size 2


  if (a_naked_triple == 3) {
    for (auto& p_friend : p_friends) {
      unsigned int matches = 0;
      for (auto& value : p_friend->possible_values) {
        if (third_value_to_remove == value || value_exists(value)) {
          matches++;
        }
      }

      // It is ok to remove values from the brick if does not belong to the naked_triple
      if (matches > 0 && matches != p_friend->possible_values.size()) {
        for (auto& value : possible_values) {
          naked_triple_removed |= p_friend->remove_value(value);
        }
        naked_triple_removed |= p_friend->remove_value(third_value_to_remove);
      }
    }
  } // for each friend

  return naked_triple_removed;
}


// ********************************************************************************
// A hidden pair is found if two cells have two values in common and these two
// values can't be found in any other cell of the group. A group == row|col|square
// The two cells with the hidden pair have more values and these values can be
// removed with out problems.
// ********************************************************************************
bool Brick::find_hidden_pair() {
  bool hidden_pair_found = false;
  if (possible_values.size() > 2) {
    hidden_pair_found |= remove_hidden_pair_from_friends(square_friends);
    hidden_pair_found |= remove_hidden_pair_from_friends(row_friends);
    hidden_pair_found |= remove_hidden_pair_from_friends(col_friends);
  }
  return hidden_pair_found;
}

bool Brick::remove_hidden_pair_from_friends(vector<Brick*>& p_friends) {
  bool hidden_pair_removed = false;
  int exactly_two_values = 0;

  // Looping over the friends
  for (auto& p_friend : p_friends) {

    // Checking against the possible values of the friend and this cell
    for (const auto& value : possible_values) {
      if (p_friend->value_exists(value)) {
        exactly_two_values++;
        if (exactly_two_values > 2) break; // No need to continue if more hits
      }
    }

    // A cell with only one hit or more than 2 hits can't exist
    // together with a hidden pair of these two values
    if (exactly_two_values == 1 || exactly_two_values > 2) {
      break;
    }

  }

  // If we at this stage have exactly two equal values then
  // we have found a hidden pair.
  // (The if statement inside the loop takes care cells with only one matched value)
  if (exactly_two_values == 2) {
    hidden_pair_removed = true;
  }

  return hidden_pair_removed;
}

bool Brick::find_locked_candidate_in_square() {
  bool found_locked_candidate = false;

  if (possible_values.size() > 1) {
    found_locked_candidate |= remove_locked_candidate_in_square(row_friends);
    found_locked_candidate |= remove_locked_candidate_in_square(col_friends);
  }

  return found_locked_candidate;
}

bool Brick::remove_locked_candidate_in_square(vector<Brick*>& p_friends) {
  bool locked_candidate_removed = false;
  vector<Brick*> friend_intersect; // Friends that intersect with p_friends and square_friends

  // A brick will always have 2 intersecting friends
  friend_intersect.reserve(2);

  // Finding the two intersecting friends
  for (auto& p_friend : p_friends) {
    for (auto& s_friend : square_friends) {
      if (p_friend == s_friend) {  // Compare the Brick* == Brick*
        friend_intersect.push_back(p_friend);
      }
    }
  }

  for (auto& value : possible_values) {
    int value_to_remove = -1;
    for (auto& s_friend : square_friends) {
      if (s_friend != friend_intersect[0] &&
          s_friend != friend_intersect[1] &&
          s_friend->value_exists(value)) {
        value_to_remove = -1;
        break; // We found the value in the square
      }
      else if (s_friend->value_exists(value)) {
        value_to_remove = value;
      }
    } // for each friend

    // Check if we have a locked candidate and remove it
    if (value_to_remove != -1) {
      for (auto& p_friend : p_friends) {
        if (p_friend != friend_intersect[0] && p_friend != friend_intersect[1]) {
          locked_candidate_removed |= p_friend->remove_value(value_to_remove);
        }
      }
    }

  } // for each possible value

  return locked_candidate_removed;
}

bool Brick::find_locked_candidate_in_row_or_col() {
  bool found_locked_candidate = false;

  if (possible_values.size() > 1) {
    found_locked_candidate |= remove_locked_candidate_in_row_or_col(row_friends);
    found_locked_candidate |= remove_locked_candidate_in_row_or_col(col_friends);
  }

  return found_locked_candidate;
}

bool Brick::remove_locked_candidate_in_row_or_col(vector<Brick*>& p_friends) {
  bool locked_candidate_removed = false;
  vector<Brick*> friend_intersect; // Friends that intersect with p_friends and square_friends

  // A brick will always have 2 intersecting friends
  friend_intersect.reserve(2);

  // Finding the two intersecting friends
  for (auto& p_friend : p_friends) {
    for (auto& s_friend : square_friends) {
      if (p_friend == s_friend) { // Comparing Brick* == Brick*
        friend_intersect.push_back(p_friend);
      }
    }
  }

  for (auto& value : possible_values) {
    int value_to_remove_from_square = -1;
    for (auto& p_friend : p_friends) {
      if (p_friend != friend_intersect[0] &&
          p_friend != friend_intersect[1] &&
          p_friend->value_exists(value)) {
        value_to_remove_from_square = -1;
        break; // We found the value outside the square
      }
      else if (p_friend->value_exists(value)) {
        value_to_remove_from_square = value;
      }
    } // for each friend

    // Check if we have a locked candidate and remove it from the square
    if (value_to_remove_from_square != -1) {
      for (auto& s_friend : square_friends) {
        if (s_friend != friend_intersect[0] && s_friend != friend_intersect[1]) {
          locked_candidate_removed |= s_friend->remove_value(value_to_remove_from_square);
        }
      }
    }

  } // for each possible value

  return locked_candidate_removed;
}
