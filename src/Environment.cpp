/*
* Environment.cpp
*
*  Created on: 26 feb 2014
*      Author: strimmel
*/

#include "Environment.h"

using namespace std;

Environment::Environment(string t_p_path) :
test_pattern_path(t_p_path), board(), solver(board), board_index(1) {
}

Environment::~Environment() { }

void Environment::run() {

  // ***************************************************************************
  // User input from console
  // ***************************************************************************
  int input_value = 0;
  int boards_to_generate = 0;
  cout << "Choose which board input to use:" << endl;
  cout << "1. Let the generator generate the boards" << endl;
  cout << "2. Use the " << test_pattern_path << " file" << endl;
  // TODO use some type of function instead of these two very equal loops
  while (true) {
    cout << "Your selection [1|2]: ";
    string input_str;
    getline(cin, input_str);
    if (!(stringstream(input_str) >> input_value)) cout << "Bad input!" << endl;
    else if (input_value == 1) {
      p_generator = make_shared<RandomGenerator>(board, solver);
      break; // Break the while-loop
    }
    else if (input_value == 2) {
      p_generator = make_shared<InputFromFile>(board, test_pattern_path);
      break; // Break the while-loop
    }
    else cout << input_value << " isn't a valid selection" << endl;
  }

  // TODO use some type of function for this
  while (true) {
    cout << "How many boards to run [POS. INTEGER]? ";
    string input_str;
    getline(cin, input_str);
    if (!(stringstream(input_str) >> boards_to_generate)) cout << "Bad input!" << endl;
    else if (boards_to_generate > 0) {
      p_generator->set_boards_to_generate(boards_to_generate);
      break; // Break the while-loop
    }
    else cout << boards_to_generate << " isn't a valid selection" << endl;
  }
  // ***************************************************************************


  board_index = 1;

  // Loop that does the board generation and solving
  while (true) {

    // Generate a board, if function returns false then we are finished
    if (!p_generator->get_board()) break;

    // Print unsolved board
    cout << "Board to solve " << board_index << ":" << endl;
    cout << board.print_board();

    // Solve the board
    solver.solve();

    // Print how many iterations the solver needed to solve this board
    cout << solver.iterations_needed();

    // Print the solved board
    cout << solver.print_solution();

    // Increment board index
    board_index++;
  }
}