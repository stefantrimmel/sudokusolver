/*
* Brick.h
*
*  Created on: 14 feb 2014
*      Author: strimmel
*/

#ifndef BRICK_H_
#define BRICK_H_

#include <string>
#include <vector>
#include <list>
#include <sstream>
#include <iostream>
#include <cstdlib>  // # abs


class Brick {
private:
  int                 row;             // Mainly to be able to print out which brick this is (debug)
  int                 col;             // Mainly to be able to print out which brick this is (debug)
  std::list<int>      possible_values; // Holds the possible values that this brick can have
  std::vector<Brick*> square_friends;  // Vector with brick pointers to the rest of the square
  std::vector<Brick*> row_friends;     // Vector with brick pointers to the rest of the row
  std::vector<Brick*> col_friends;     // Vector with brick pointers to the rest of the column

  // ##########################################
  // ##### Functions operating on friends #####
  // #####    Virtual because of GMOCK    #####
  // ##########################################
  virtual bool remove_value_square_friends(int value);
  virtual bool remove_value_row_friends(int value);
  virtual bool remove_value_col_friends(int value);
  virtual bool value_exists_square_friends(int value) const;
  virtual bool value_exists_row_friends(int value) const;
  virtual bool value_exists_col_friends(int value) const;
  virtual bool value_exists_in_friends(int value, const std::vector<Brick*>& p_friends) const;
  virtual bool remove_naked_pair_from_friends(std::vector<Brick*>& p_friends);
  virtual bool remove_naked_triple_from_friends(std::vector<Brick*>& p_friends);
  virtual bool remove_hidden_pair_from_friends(std::vector<Brick*>& p_friends);
  virtual bool remove_locked_candidate_in_row_or_col(std::vector<Brick*>& p_friends);
  virtual bool remove_locked_candidate_in_square(std::vector<Brick*>& p_friends);

public:
  Brick(int row, int col);
  virtual ~Brick();        // Virtual because of GMOCK

  // #########################################
  // ##### Functions operating on itself #####
  // #####    Virtual because of GMOCK   #####
  // #########################################
  virtual void set_correct_value(int value);
  virtual void add_value(int value); // <-- Just for debug, testing
  virtual bool remove_value(int value);                   
  virtual bool value_exists(int value) const;            
  virtual bool solved() const;                           
  virtual bool no_possible_values() const;               
  virtual int get_value_int() const;                     
  virtual std::string get_value_str() const;             
  virtual std::string get_all_values_str() const;
  virtual std::string get_name() const;                  

  // ##########################################
  // ##### Functions operating on friends #####
  // #####    Virtual because of GMOCK    #####
  // ##########################################
  virtual void add_square_friend(Brick* brick);          
  virtual void add_row_friend(Brick* brick);             
  virtual void add_col_friend(Brick* brick);             
  virtual bool remove_value_from_friends(int value);     
  virtual bool remove_single_from_friends();             
  virtual bool find_hidden_single();                     
  virtual bool find_naked_pair();                        
  virtual bool find_naked_triple();                      
  virtual bool find_hidden_pair();                       
  virtual bool find_locked_candidate_in_square();        
  virtual bool find_locked_candidate_in_row_or_col();    
};

#endif /* BRICK_H_ */

