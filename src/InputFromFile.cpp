/*
* InputFromFile.cpp
*
*  Created on: 24 feb 2014
*      Author: strimmel
*/

#include "InputFromFile.h"

using namespace std;

InputFromFile::InputFromFile(Board& board, string filename) :
  Generator(board), filename(filename), infile(filename) {
}

bool InputFromFile::get_board() {

  bool filling_successful = false; // This bool is return by this function, it will return false if the infile didn't have a new board.

  // Resetting the board to the starting point (all bricks are reseted)
  board.reset();

  // Getting a reference to the bricks
  vector<Brick>& bricks = board.get_bricks();

  string line;
  int position = 0;
  while (getline(infile, line)) {
    if (line.empty()) continue; // Skip empty lines
    if (line[0] == '#' || line[0] == '-' || line[0] == '/' || line[0] == '%') continue; // Skip comment lines
    istringstream iss(line);
    int value;

    // If we have an OK integer we add it to the current position
    while (iss >> value && value >= 0 && value <= 9) {
      if (value != 0) {
        bricks[position].set_correct_value(value);
      }
      position++;
      if (position > 80) break; // break istringstream loop
    }

    if (position > 80) break; // break getline loop

  }

  // Check if it was possible to fill a new board
  if (position > 80) {
    filling_successful = true;
  }

  // We need to calculate the return value before incrementing num_generated_boards
  bool return_val = (filling_successful && !finished());
  
  // Increment number of generated boards
  num_generated_boards++;

  return return_val;
}

InputFromFile::~InputFromFile() {
  infile.close();
}
