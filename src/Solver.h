/*
* Solver.h
*
*  Created on: 18 feb 2014
*      Author: strimmel
*/

#ifndef SOLVER_H_
#define SOLVER_H_

#include <iostream>
#include <string>

#include "Board.h"


class Solver {
private:
  Board& board;
  int iterations_used;
public:
  Solver(Board& board);
  virtual ~Solver();                        // Virtual because of GMOCK
  virtual bool solve();                     // Virtual because of GMOCK
  virtual bool is_solved();                 // Virtual because of GMOCK
  virtual std::string iterations_needed();  // Virtual because of GMOCK
  virtual std::string print_solution();     // Virtual because of GMOCK
};

#endif /* SOLVER_H_ */
