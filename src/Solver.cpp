/*
* Solver.cpp
*
*  Created on: 18 feb 2014
*      Author: strimmel
*/

#include "Solver.h"

using namespace std;

Solver::Solver(Board& board) : board(board), iterations_used(0) { }

Solver::~Solver() { }

bool Solver::solve() {
  bool board_solved = false;
  bool possible_to_solve = true;
  bool board_changed_this_iteration = false;
  iterations_used = 0;

  // Get the bricks from the board
  auto& bricks = board.get_bricks();

  while (possible_to_solve && !board_solved) {

    // Setting it to true, if no brick changes it then we are finished
    board_solved = true;
    board_changed_this_iteration = false;

    // Iterate over all bricks in the board
    for (auto& brick : bricks) {

      // If this brick has a single value (solved) then we can
      // remove this value from its square, row and column.
      board_changed_this_iteration |= brick.remove_single_from_friends();

      // If this brick has a value among the possible values that only
      // exists in this square, row or column then it must be in this brick.
      board_changed_this_iteration |= brick.find_hidden_single();

      // If two bricks have same two possible values then
      // these two values can be removed from the other bricks.
      board_changed_this_iteration |= brick.find_naked_pair();

      // If three bricks contains a perfect set or subset of three possible values then
      // these values can be removed from the other bricks.
      board_changed_this_iteration |= brick.find_naked_triple();

      // If two bricks share two possible values (and some other values) and
      // these values can't be found in the other cells. The two cells
      // can be striped down to only contain these two values.
      board_changed_this_iteration |= brick.find_hidden_pair();

      // If a value in a square only exist on one row/col then it can
      // be removed in the cells of this row/col.
      board_changed_this_iteration |= brick.find_locked_candidate_in_square();

      // If a value in a col/row only exist in the cells of a square then
      // this value can be removed from that square cells which are not in
      // the col/row.
      board_changed_this_iteration |= brick.find_locked_candidate_in_row_or_col();

      // If at least one brick is not solved then we are not finished
      board_solved &= brick.solved();

      iterations_used++;

      if (brick.no_possible_values()) {
        board_solved = false;
        possible_to_solve = false;
        cout << "ERROR: Not able to solve board because " << brick.get_name() << " has no possible values" << endl;
        // TODO Throw exception
        break;
      }

    } // for each brick in board

    // If nothing changed then nothing will change next time either
    // Either we are finished or there are some problem with the board.
    if (!board_changed_this_iteration) {
      break;
    }

  } // while board not solved

  return board_solved;
}


// I know that this could be done a lot faster (less iterations)
bool Solver::is_solved() {
  return board.solved();
}

string Solver::iterations_needed() {
  string str = "";
  str += "Solver used " + to_string(iterations_used) + " iterations.\n";
  return str;
}

string Solver::print_solution() {
  string str = "";

  str += "\nSolution:\n";
  if (board.solved()) {
    str += board.print_board();
    str += "\n\n";
  }
  else {
    str += "Solver was not possible to find the solution for this board :(\n";
    str += board.print_board_all_values();
    str += "\n\n";
  }

  return str;
}

