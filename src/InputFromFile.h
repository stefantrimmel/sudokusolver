/*
* InputFromFile.h
*
*  Created on: 24 feb 2014
*      Author: strimmel
*/

#ifndef INPUTFROMFILE_H_
#define INPUTFROMFILE_H_

#include <sstream>
#include <string>
#include <fstream>

#include "Generator.h"
#include "Board.h"


class InputFromFile : public Generator {
private:
  std::string filename;
  std::ifstream infile;
public:
  InputFromFile(Board& board, std::string filename);
  virtual ~InputFromFile();                      // Virtual because of GMOCK
  virtual bool get_board();                      // Virtual because of GMOCK
};

#endif /* INPUTFROMFILE_H_ */
