// SudukoSolver.cpp : Defines the entry point for the console application.
//

#include <string>

#include "Environment.h"

using namespace std;

int main(int argc, char* argv[])
{
  string test_pattern_path = R"(C:\Users\strimmel\git\SudokuSolver\Debug\test_patterns.txt)";

  // Create the environment
  Environment env(test_pattern_path);

  // Start the environment
  env.run();

  return 0;
}

