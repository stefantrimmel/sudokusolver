/*
* Generator.h
*
*  Created on: 24 feb 2014
*      Author: strimmel
*/

#ifndef GENERATOR_H_
#define GENERATOR_H_

#include "Board.h"


class Generator {
protected:
  Board& board;
  unsigned int boards_to_generate;   // <-- How many boards should be generated
  unsigned int num_generated_boards; // <-- How many boards have been generated so far
public:
  Generator(Board& board);
  virtual ~Generator();
  virtual void set_boards_to_generate(unsigned int b_to_gen);   // Virtual because of GMOCK
  virtual bool finished() const;                                // Virtual because of GMOCK
  virtual bool get_board();
};

#endif /* GENERATOR_H_ */