/*
* Board.cpp
*
*  Created on: 14 feb 2014
*      Author: strimmel
*/

#include "Board.h"

using namespace std;

Board::Board() {

  // Setting the board size (allocating the space needed)
  bricks.reserve(81); // 9 rows and 9 columns == 81 bricks

  // Creating all bricks
  create_bricks();

  // Setting all friends for each brick
  set_all_brick_friends();

}

Board::~Board() {}

void Board::create_bricks() {
  int row = 0;
  int col = 0;
  for (int i = 0; i < 81; i++) {

    // The new brick, <row, col> is just for printout <debug>
    Brick new_brick(row, col);
    bricks.push_back(new_brick);

    col++;
    if (col >= 9) {
      col = 0;
      row++;
    }
  }
}

void Board::set_all_brick_friends() {
  int row = 0;
  int col = 0;
  for (auto& brick : bricks) {
    
    // Adding all column-friends for this brick
    for (int row_index = 0; row_index < 9; row_index++) {
      // We don't want to create a friend to ourself
      if (row != row_index) brick.add_col_friend(&bricks[col + 9 * row_index]);
    }

    // Adding all row-friends for this brick
    for (int col_index = 0; col_index < 9; col_index++) {
      // Don't create friend with itself
      if (col != col_index) brick.add_row_friend(&bricks[row * 9 + col_index]);
    }

    // Adding all square-friends for this brick
    int first_pos = first_pos_in_square(row, col);
    // 1st row in square
    for (int i = 0; i < 3; i++) {
      if ((first_pos + i) != (col + row * 9)) {
        brick.add_square_friend(&bricks[first_pos + i]);
      }
    }
    // 2nd row in square
    for (int i = 0; i < 3; i++) {
      if ((first_pos + 9 + i) != (col + row * 9)) {
        brick.add_square_friend(&bricks[first_pos + 9 + i]);
      }
    }
    // 3rd row in square
    for (int i = 0; i < 3; i++) {
      if ((first_pos + 18 + i) != (col + row * 9)) {
        brick.add_square_friend(&bricks[first_pos + 18 + i]);
      }
    }

    col++;
    if (col >= 9) {
      col = 0;
      row++;
    }


  } // for each brick in bricks
}

// Returns a string of the board
string Board::print_board() {
  string str;

  // Adding first boarder-line
  str += "#####################################\n";
  int row = 0;
  int col = 0;
  for (auto& brick : bricks) {

    // Add the boarder and a space before the integer
    switch (col) {
    case 0: case 3: case 6:
      str += "# "; break;
    case 1: case 2: case 4: case 5: case 7: case 8:
      str += "| "; break;
    }

    // Add the integer and a space
    if (brick.solved()) {
      str += brick.get_value_str();
      str += " ";
    }
    else {
      str += "  ";
    }

    col++;
    if (col >= 9) {
      // Add the last boarder and
      str += "#\n";
      col = 0;
      row++;
      // Add the boarder-line
      if ((row % 3) == 0) {
        str += "#####################################\n";
      }
      else {
        str += "#---+---+---#---+---+---#---+---+---#\n";
      }

    }
  }

  return str;
}

void Board::reset() {
  for (auto& brick : bricks) {
    brick.set_correct_value(1);
    for (int i = 2; i < 10; i++) {
      brick.add_value(i);
    }
  }
}

// Returns a string of the board
string Board::print_board_all_values() {
  string str;

  // Adding first boarder-line
  str += "#############################################################################################################\n";
  int row = 0;
  int col = 0;
  for (auto& brick : bricks) {

    // Add the boarder and a space before the integer
    switch (col) {
    case 0: case 3: case 6:
      str += "# "; break;
    case 1: case 2: case 4: case 5: case 7: case 8:
      str += "| "; break;
    }

    // Add the integers and a space
    str += brick.get_all_values_str();
    str += " ";

    col++;
    if (col >= 9) {
      // Add the last boarder and
      str += "#\n";
      col = 0;
      row++;
      // Add the boarder-line
      if ((row % 3) == 0) {
        str += "#############################################################################################################\n";
      }
      else {
        str += "#-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------#\n";
      }

    }
  }

  return str;
}

// Returns the bricks vector
vector<Brick>& Board::get_bricks() {
  return bricks;
}

// Returns which square that a <row, col> belongs to
int Board::calc_square(int row, int col) {

  // Checking input boundaries
  if (row > 8 || row < 0) return -1;
  if (col > 8 || col < 0) return -1;

  // Calculate vector index from the col and row
  int vector_index = col + 9 * row;

  return vector_index_to_square(vector_index);

}

// Returns the first <row, col> in a square give any <row, col> in that square
int Board::first_pos_in_square(int row, int col) {
  // Checking input boundaries
  if (row > 8 || row < 0) return -1;
  if (col > 8 || col < 0) return -1;

  int square_nr = calc_square(row, col);

  return first_pos_in_square(square_nr);
}

// Returns the first <row, col> in a square given the square number
int Board::first_pos_in_square(int square_nr) {
  if (square_nr > 8 || square_nr < 0) return -1;

  int first_pos;

  switch (square_nr) {
  case 0: first_pos = 0; break;
  case 1: first_pos = 3; break;
  case 2: first_pos = 6; break;
  case 3: first_pos = 27; break;
  case 4: first_pos = 30; break;
  case 5: first_pos = 33; break;
  case 6: first_pos = 54; break;
  case 7: first_pos = 57; break;
  case 8: first_pos = 60; break;
  default: first_pos = 0; break;
  }

  return first_pos;
}

// Converts a vector index to a square number
int Board::vector_index_to_square(int vector_index) {

  int square_nr;

  switch (vector_index) {
  case  0: case  1: case  2:
  case  9: case 10: case 11:
  case 18: case 19: case 20:
    square_nr = 0; break;
  case  3: case  4: case  5:
  case 12: case 13: case 14:
  case 21: case 22: case 23:
    square_nr = 1; break;
  case  6: case  7: case  8:
  case 15: case 16: case 17:
  case 24: case 25: case 26:
    square_nr = 2; break;
  case 27: case 28: case 29:
  case 36: case 37: case 38:
  case 45: case 46: case 47:
    square_nr = 3; break;
  case 30: case 31: case 32:
  case 39: case 40: case 41:
  case 48: case 49: case 50:
    square_nr = 4; break;
  case 33: case 34: case 35:
  case 42: case 43: case 44:
  case 51: case 52: case 53:
    square_nr = 5; break;
  case 54: case 55: case 56:
  case 63: case 64: case 65:
  case 72: case 73: case 74:
    square_nr = 6; break;
  case 57: case 58: case 59:
  case 66: case 67: case 68:
  case 75: case 76: case 77:
    square_nr = 7; break;
  case 60: case 61: case 62:
  case 69: case 70: case 71:
  case 78: case 79: case 80:
    square_nr = 8; break;
  default: square_nr = 0; break;
  }

  return square_nr;

}

bool Board::solved() {
  const int magic_sum = 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9; // 45

  // Make sure that all brick have only one value
  for (const auto& brick : bricks) {
    if (!brick.solved()) return false;
  }

  // Check all rows
  for (int row = 0; row < 9; row++) {
    int sum = 0;
    for (int col = 0; col < 9; col++) {
      sum += bricks[row * 9 + col].get_value_int();
    }
    if (magic_sum != sum) return false;
  }

  // Check all columns
  for (int col = 0; col < 9; col++) {
    int sum = 0;
    for (int row = 0; row < 9; row++) {
      sum += bricks[col + row * 9].get_value_int();
    }
    if (magic_sum != sum) return false;
  }

  // Check all squares
  for (int square = 0; square < 9; square++) {
    int sum = 0;
    for (int row = 0; row < 3; row++) {
      for (int col = 0; col < 3; col++) {
        sum += bricks[first_pos_in_square(square) + row * 9 + col].get_value_int();
      }
    }
    if (magic_sum != sum) return false;
  }

  return true;
}
