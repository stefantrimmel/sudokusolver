#include "Generator.h"

Generator::Generator(Board& board) : board(board), boards_to_generate(0), num_generated_boards(0) {}

Generator::~Generator() {}

bool Generator::get_board() {
  // This is just a base class dummy
  num_generated_boards++;
  return finished();
}

void Generator::set_boards_to_generate(unsigned int b_to_gen) {
  boards_to_generate = b_to_gen;
}

bool Generator::finished() const {
  return (num_generated_boards >= boards_to_generate);
}
